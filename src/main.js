import Vue from 'vue'
import App from './App.vue'
import Actions from './components/Actions/index'
import PictureInput from 'vue-picture-input'
Vue.component('actions', Actions)
Vue.component('picture-input',PictureInput)

new Vue({
  el: '#app',
  render: h => h(App)
})
